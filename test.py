"""test suite for StringMethods"""
import unittest
import transform


class TestStringMethods(unittest.TestCase):
    """StringMethods test suite"""

    def test_is_upper(self):
        """test string to upper case"""
        sting = transform.to_upper_case("hello")
        self.assertEqual(sting, "HELLO")

    def test_is_lower(self):
        """test string to lower case"""
        sting = transform.to_lower_case("HELLO")
        self.assertEqual(sting, "hello")

    def test_is_capitalize(self):
        """test string capitalized"""
        sting = transform.to_capitalize("HELLO")
        self.assertEqual(sting, "Hello")


if __name__ == '__main__':
    unittest.main()
